# Changelog

## [0.1.1] - 2023-04-26
* Fix default value of variable `image_type`

## [0.1.0] - 2023-04-21
* Initial version
