## Objective

This template provides a complete pipeline for testing, building, and deploying
a JavaScript project using NPM.

**Important notes:**
- This template use the [new GitLab component](https://docs.gitlab.com/ee/architecture/blueprints/ci_pipeline_components/) syntax
- This template come from the **incubator** repository. It is not considered as
production-ready.

The pipeline consists of the following stages:
1. **Testing**: Run `lint` and `test` from your `package.json` to ensure the code works as expected
1. **Building**: Bundle the code and dependencies into a production-ready package
1. **Deploying**: Deploy the package to GitLab pages

## How to use it

- Ensure your project has a `package.json` file containing `lint`, `test` and `build` scripts
- Copy/paste **Quick use** above in file `.gitlab-ci.yml` at the root of your repository

## Variables

As this template use the [new GitLab
component](https://docs.gitlab.com/ee/architecture/blueprints/ci_pipeline_components/)
syntax, you need to use `with` keyword to set input parameters. Example:

```yaml
include:
  - remote: 'https://api.r2devops.io/job/r/gitlab/r2devops/incubator/node@latest.yaml'
    with:
      node_version: "16"
      npm_install_options: "--my-option"
      output directory: "output"
```


The pipeline can be configured by setting the following input parameters:

| Variable Name          | Description                                       | Default Value |
|------------------------|---------------------------------------------------|---------------|
| `project_root`         | The path to the root directory of your project.   | `"."`         |
| `node_version`         | The version of Node.js to use in the pipeline.    | `"20"`        |
| `image_type`           | The type of Docker image to use for the pipeline. | `"buster"`   |
| `output_directory`     | The path to the directory where the production-ready code will be placed after the build process. | `"build"` |
| `npm_install_options`  | The options to pass to the `npm install` command. | `""`          |
| `npm_lint_options`     | The options to pass to the `npm run lint` command.| `""`          |
| `npm_test_options`     | The options to pass to the `npm run test` command.| `""`          |
| `npm_build_options`    | The options to pass to the `npm run build` command.| `""`          |
