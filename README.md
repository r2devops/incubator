# Incubator

Welcome to the Incubator project! This project contains Continuous Integration
and Continuous Deployment (CI/CD) templates for the R2Devops platform, which
are currently in development and not yet ready for production use.

## Purpose

The purpose of the Incubator project is to provide a space for R2Devops
developers and users to experiment with and contribute to new CI/CD templates
before they are released into production.

This helps to ensure that new templates are thoroughly tested and meet the
needs of the R2Devops community.

## Getting Started

To get started with the Incubator project, simply clone the repository and
explore the available templates. Please note that the templates in this project
are not yet ready for production use and may contain bugs or other issues.

## Contributing

We welcome contributions to the Incubator project! If you are interested in
contributing, please review the contribution guidelines [in the
documentation](https://docs.r2devops.io/public-catalog/contribute/) and follow
the instructions for submitting a pull request.

## Feedback and Support

If you have any feedback or encounter any issues with the Incubator project,
please open an issue in the repository or contact the R2Devops team on
[Discord](https://discord.r2devops.io/) for support.
